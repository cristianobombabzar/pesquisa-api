CREATE TABLE cadastro.company_address(

  id serial not null,
  company_id integer not null,
  county_id integer not null,
  street character varying(50) not null,
  number character varying(10) not null,
  cep character varying(10) not null,
  district character varying(20) not null,
  complement text,

  CONSTRAINT pk_company_address_company PRIMARY KEY (id),
  CONSTRAINT company_address_fk_company FOREIGN KEY (company_id)
    REFERENCES cadastro.company (id),
  CONSTRAINT company_address_fk_county FOREIGN KEY (county_id)
    REFERENCES cadastro.company (id)
);


INSERT INTO cadastro.company_address (id, company_id, county_id, street, number, cep, district, complement) VALUES (1, 1, 1, 'street 1', 'S/N', '887.50-000', 'São Basílio 1', null);
INSERT INTO cadastro.company_address (id, company_id, county_id, street, number, cep, district, complement) VALUES (2, 2, 2, 'street 2', 'S/N', '887.50-000', 'São Basílio 2', null);
INSERT INTO cadastro.company_address (id, company_id, county_id, street, number, cep, district, complement) VALUES (3, 3, 3, 'street 3', 'S/N', '887.50-000', 'São Basílio 3', null);
INSERT INTO cadastro.company_address (id, company_id, county_id, street, number, cep, district, complement) VALUES (4, 4, 4, 'street 4', 'S/N', '887.50-000', 'São Basílio 4', null);
INSERT INTO cadastro.company_address (id, company_id, county_id, street, number, cep, district, complement) VALUES (5, 5, 5, 'street 5', 'S/N', '887.50-000', 'São Basílio 5', null);
INSERT INTO cadastro.company_address (id, company_id, county_id, street, number, cep, district, complement) VALUES (6, 6, 6, 'street 6', 'S/N', '887.50-000', 'São Basílio 6', null);
INSERT INTO cadastro.company_address (id, company_id, county_id, street, number, cep, district, complement) VALUES (7, 7, 7, 'street 7', 'S/N', '887.50-000', 'São Basílio 7', null);
INSERT INTO cadastro.company_address (id, company_id, county_id, street, number, cep, district, complement) VALUES (8, 8, 8, 'street 8', 'S/N', '887.50-000', 'São Basílio 8', null);
INSERT INTO cadastro.company_address (id, company_id, county_id, street, number, cep, district, complement) VALUES (9, 9, 9, 'street 9', 'S/N', '887.50-000', 'São Basílio 9', null);
INSERT INTO cadastro.company_address (id, company_id, county_id, street, number, cep, district, complement) VALUES (10, 10, 10, 'street 10', 'S/N', '887.50-000', 'São Basílio 10', null);

SELECT setval('cadastro.company_address_id_seq', 10, true);