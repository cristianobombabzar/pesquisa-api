CREATE TABLE cadastro.county(
	id serial NOT NULL,
	name character varying(50) NOT NULL,
	uf character(2) NOT NULL,
	CONSTRAINT pk_county PRIMARY KEY (id)

);

INSERT INTO cadastro.county(id, name, uf) VALUES (1, 'BRAÇO DO NORTE', 'SC');
INSERT INTO cadastro.county(id, name, uf) VALUES (2, 'SÃO LUDGERO', 'SC');
INSERT INTO cadastro.county(id, name, uf) VALUES (3, 'ORLEANS', 'SC');
INSERT INTO cadastro.county(id, name, uf) VALUES (4, 'GRAVATAL', 'SC');
INSERT INTO cadastro.county(id, name, uf) VALUES (5, 'TUBARÃO', 'SC');
INSERT INTO cadastro.county(id, name, uf) VALUES (6, 'TORRES', 'RS');
INSERT INTO cadastro.county(id, name, uf) VALUES (7, 'RIO DE JANEIRO', 'RJ');
INSERT INTO cadastro.county(id, name, uf) VALUES (8, 'SÃO PAULO', 'SP');
INSERT INTO cadastro.county(id, name, uf) VALUES (9, 'PORTO ALEGRE', 'RS');
INSERT INTO cadastro.county(id, name, uf) VALUES (10, 'CRICIÚMA', 'SC');


SELECT setval('cadastro.county_id_seq', 10, true);