CREATE TABLE security.user_permission (
  id serial not null,
	user_id integer NOT NULL,
	permission_id integer NOT NULL,
	CONSTRAINT pk_user_permission PRIMARY KEY (user_id, permission_id),
	CONSTRAINT unique_user_permission UNIQUE (user_id, permission_id),
	CONSTRAINT user_permission_fk_user FOREIGN KEY (user_id) REFERENCES security.user(id),
  CONSTRAINT user_permission_fk_permission FOREIGN KEY (permission_id) REFERENCES security.permission(id)
);