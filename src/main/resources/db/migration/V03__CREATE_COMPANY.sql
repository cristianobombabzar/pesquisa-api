CREATE TABLE cadastro.company(
	id serial NOT NULL,
	name character varying(50) NOT NULL,
	type smallint NOT NULL,
	inscription character varying(18) NOT NULL,
	date_added date NOT NULL,
	date_update timestamp NOT NULL,
	active boolean NOT NULL,
	CONSTRAINT pk_company PRIMARY KEY (id)
);

COMMENT ON COLUMN cadastro.company.type IS '1 - FISICA, 2 - JURÍDICA';
COMMENT ON COLUMN cadastro.company.inscription IS 'cpf ou cnpj';

INSERT INTO cadastro.company(id, name, type, inscription, date_added, date_update, active) VALUES (1, 'LOGPRO', 2, '67.342.821/0001-87', NOW(), NOW(), true);
INSERT INTO cadastro.company(id, name, type, inscription, date_added, date_update, active) VALUES (2, 'FOLHA DO VALE', 2, '64.014.563/0001-11', NOW(), NOW(), true);
INSERT INTO cadastro.company(id, name, type, inscription, date_added, date_update, active) VALUES (3, 'SUPERMERCADO BECKER', 2, '00.473.934/0001-40', NOW(), NOW(), true);
INSERT INTO cadastro.company(id, name, type, inscription, date_added, date_update, active) VALUES (4, 'ODONTO X', 2, '94.768.527/0001-04', NOW(), NOW(), true);
INSERT INTO cadastro.company(id, name, type, inscription, date_added, date_update, active) VALUES (5, 'SÉRGIO FUNILARIA', 2, '77.145.489/0001-14', NOW(), NOW(), true);
INSERT INTO cadastro.company(id, name, type, inscription, date_added, date_update, active) VALUES (6, 'ELETROJO SERVIÇOS ELETRICOS', 2, '30.471.887/0001-67', NOW(), NOW(), false);
INSERT INTO cadastro.company(id, name, type, inscription, date_added, date_update, active) VALUES (7, 'JCS INFORMÁTICA', 2, '61.833.796/0001-58', NOW(), NOW(), true);
INSERT INTO cadastro.company(id, name, type, inscription, date_added, date_update, active) VALUES (8, 'TRIER SISTEMAS', 2, '15.688.232/0001-51', NOW(), NOW(), true);
INSERT INTO cadastro.company(id, name, type, inscription, date_added, date_update, active) VALUES (9, 'SAMAE BLUMENAU', 2, '24.918.126/0001-08', NOW(), NOW(), false);
INSERT INTO cadastro.company(id, name, type, inscription, date_added, date_update, active) VALUES (10, 'SAMAE URUSSANGA', 2, '70.675.486/0001-43', NOW(), NOW(), false);

SELECT setval('cadastro.company_id_seq', 10, true);