CREATE TABLE security.permission(
	id serial NOT NULL,
	description text NOT NULL,
	active boolean NOT NULL,
	CONSTRAINT pk_permission PRIMARY KEY (id)
);