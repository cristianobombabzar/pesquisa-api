CREATE TABLE security.user(
	id serial NOT NULL,
	name text NOT NULL,
	email text NOT NULL,
	login text NOT NULL,
	password text NOT NULL,
	active boolean NOT NULL,
	company_id integer NOT NULL,
	CONSTRAINT pk_usuario PRIMARY KEY (id)
);

ALTER TABLE security.user ADD CONSTRAINT user_fk_company FOREIGN KEY (id) REFERENCES cadastro.company (id);

INSERT INTO security.user(id, name, email, login, password, active, company_id) VALUES (1, 'admin logpro','admin.logpro', 'admin@logpro.com.br', '$2a$10$X607ZPhQ4EgGNaYKt3n4SONjIv9zc.VMWdEuhCuba7oLAL5IvcL5.', true, 1);
INSERT INTO security.user(id, name, email, login, password, active, company_id) VALUES (2, 'Cristiano Bombazar','cristiano.bombazar', 'cristiano@logpro.com.br', '$2a$10$X607ZPhQ4EgGNaYKt3n4SONjIv9zc.VMWdEuhCuba7oLAL5IvcL5.', true, 1);
INSERT INTO security.user(id, name, email, login, password, active, company_id) VALUES (3, 'Leandro Martins','leandro.martins', 'leandro@logpro.com.br', '$2a$10$X607ZPhQ4EgGNaYKt3n4SONjIv9zc.VMWdEuhCuba7oLAL5IvcL5.', true, 1);
INSERT INTO security.user(id, name, email, login, password, active, company_id) VALUES (4, 'Lucas Mazzuco','lucas.mazzuco', 'lucas@logpro.com.br', '$2a$10$X607ZPhQ4EgGNaYKt3n4SONjIv9zc.VMWdEuhCuba7oLAL5IvcL5.', true, 1);
INSERT INTO security.user(id, name, email, login, password, active, company_id) VALUES (5, 'Eliane Silveira','eliane.silveira', 'eliane@logpro.com.br', '$2a$10$X607ZPhQ4EgGNaYKt3n4SONjIv9zc.VMWdEuhCuba7oLAL5IvcL5.', true, 1);

INSERT INTO security.user(id, name, email, login, password, active, company_id) VALUES (6, 'admin', 'admin.folha', 'admin@folhadovale.com.br', '$2a$10$X607ZPhQ4EgGNaYKt3n4SONjIv9zc.VMWdEuhCuba7oLAL5IvcL5.', true, 2);
INSERT INTO security.user(id, name, email, login, password, active, company_id) VALUES (7, 'Marta Ramos','marta.ramos', 'Marta@folhadovale.com.br', '$2a$10$X607ZPhQ4EgGNaYKt3n4SONjIv9zc.VMWdEuhCuba7oLAL5IvcL5.', true, 2);
INSERT INTO security.user(id, name, email, login, password, active, company_id) VALUES (8, 'Cristiane Gilbert','cristiane.gilbert', 'cristiane@folhadovale.com.br', '$2a$10$X607ZPhQ4EgGNaYKt3n4SONjIv9zc.VMWdEuhCuba7oLAL5IvcL5.', true, 2);
INSERT INTO security.user(id, name, email, login, password, active, company_id) VALUES (9, 'Evandro Rorguies e Banda','evandro.rodrigues', 'evandro@folhadovale.com.br', '$2a$10$X607ZPhQ4EgGNaYKt3n4SONjIv9zc.VMWdEuhCuba7oLAL5IvcL5.', true, 2);
INSERT INTO security.user(id, name, email, login, password, active, company_id) VALUES (10, 'folha','folha1', 'folha@folhadovale.com.br', '$2a$10$X607ZPhQ4EgGNaYKt3n4SONjIv9zc.VMWdEuhCuba7oLAL5IvcL5.', true, 2);

SELECT setval('security.user_id_seq', 10, true);