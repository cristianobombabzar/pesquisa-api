package br.com.logpro.pesquisa.exception;

import jdk.nashorn.internal.runtime.regexp.joni.exception.ErrorMessages;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@ControllerAdvice
public class ExceptionHandlerAdapter extends ResponseEntityExceptionHandler {

    @Autowired
    MessageSource messageSource;

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<Error> errors = createErrorList(ex.getBindingResult(), ex);
        return handleExceptionInternal(ex, errors, headers, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseBody
        public ResponseEntity<Object> conflict(HttpServletRequest req, DataIntegrityViolationException e) {
        String messageConflit = messageSource.getMessage("message.conflit.resources", null, LocaleContextHolder.getLocale());
        List<Error> errors = createErrorList(messageConflit, e);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(errors);
    }

    private List<Error> createErrorList(BindingResult bindingResult, Exception ex){
        List<Error> errors = new ArrayList<>();
        bindingResult.getFieldErrors().forEach( fieldError -> {
            String messageUser      = messageSource.getMessage(fieldError, LocaleContextHolder.getLocale());
            String messageDeveloper = fieldError.toString();
            errors.add(Error.builder()
                            .messageUser(messageUser)
                            .messageDeveloper(messageDeveloper)
                            .build());
        });
        return errors;
    }
    private List<Error> createErrorList(String messageToUser, Exception ex){
        List<Error> errors = new ArrayList<>();
            String messageDeveloper = ex.getMessage();
            errors.add(Error.builder()
                            .messageUser(messageToUser)
                            .messageDeveloper(messageDeveloper)
                            .build());
        return errors;
    }


    @Data
    @AllArgsConstructor(access = AccessLevel.PROTECTED)
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @Builder
    private static class Error{

        String messageUser;
        String messageDeveloper;

    }
}
