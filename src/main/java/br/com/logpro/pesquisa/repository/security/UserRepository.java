package br.com.logpro.pesquisa.repository.security;

import br.com.logpro.pesquisa.filter.Filter;
import br.com.logpro.pesquisa.model.security.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>, Filter {

    @Query("SELECT usuario FROM User usuario WHERE usuario.email = ?1 OR usuario.email = ?1")
    Optional<User> findByEmailOrLogin(String login);
}
