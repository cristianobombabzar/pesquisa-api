package br.com.logpro.pesquisa.repository.cadastro;

import br.com.logpro.pesquisa.filter.Filter;
import br.com.logpro.pesquisa.model.cadastro.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Integer>, Filter {

}
