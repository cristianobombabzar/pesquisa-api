package br.com.logpro.pesquisa.repository.cadastro;

import br.com.logpro.pesquisa.filter.Filter;
import br.com.logpro.pesquisa.model.cadastro.County;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountyRepository extends JpaRepository<County, Integer>, Filter {

}
