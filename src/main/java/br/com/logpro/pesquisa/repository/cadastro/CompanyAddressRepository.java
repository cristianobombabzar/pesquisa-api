package br.com.logpro.pesquisa.repository.cadastro;

import br.com.logpro.pesquisa.model.cadastro.Company;
import br.com.logpro.pesquisa.model.cadastro.CompanyAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CompanyAddressRepository extends JpaRepository<CompanyAddress, Integer> {

    Optional<CompanyAddress> findByCompany(Company company);
}
