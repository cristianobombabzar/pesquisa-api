package br.com.logpro.pesquisa.model.cadastro;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "company_address", schema = "cadastro")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CompanyAddress {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Integer id;

    @Valid
    @NotNull
    @OneToOne
    @JsonBackReference
    private Company company;

    @NotNull
    @Size(min = 3, max = 50)
    private String street;

    @NotNull
    @Size(min = 1, max = 10)
    private String number;

    @NotNull
    @Size(min = 10, max = 10)
    private String cep;
    @NotNull
    @Size(min = 3, max = 20)
    private String district;

    private String complement;

    @ManyToOne(fetch = FetchType.EAGER)
    private County county;
}
