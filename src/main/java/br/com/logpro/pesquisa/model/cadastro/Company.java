package br.com.logpro.pesquisa.model.cadastro;

import br.com.logpro.pesquisa.model.cadastro.enuns.TypeInscription;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "company", schema = "cadastro")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Size(min = 3, max = 50)
    private String  name;

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    private TypeInscription type;

    @NotNull
    @Size(min = 14, max = 18)
    private String  inscription;

    @NotNull
    private LocalDate dateAdded;

    @NotNull
    private LocalDateTime dateUpdate;

    @NotNull
    private Boolean active;

    @OneToOne(mappedBy = "company", cascade = CascadeType.REMOVE)
    private CompanyAddress endereco;

}

