package br.com.logpro.pesquisa.model.cadastro.enuns;

public enum TypeInscription {

    DEFAULT, FISICA, JURIDICA
}
