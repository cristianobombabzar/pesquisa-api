package br.com.logpro.pesquisa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PesquisaApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PesquisaApiApplication.class, args);
	}
}
