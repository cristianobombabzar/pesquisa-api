package br.com.logpro.pesquisa.service.cadastro;

import br.com.logpro.pesquisa.model.cadastro.County;
import br.com.logpro.pesquisa.repository.cadastro.CountyRepository;
import br.com.logpro.pesquisa.service.AbstractCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CountyService extends AbstractCrudService<County, Integer> {

    private CountyRepository repository;

    @Autowired
    public CountyService(CountyRepository repository) {
        super(repository);
        this.repository = repository;
    }
}
