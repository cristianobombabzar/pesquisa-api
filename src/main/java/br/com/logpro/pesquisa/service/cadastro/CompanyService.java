package br.com.logpro.pesquisa.service.cadastro;

import br.com.logpro.pesquisa.model.cadastro.Company;
import br.com.logpro.pesquisa.repository.cadastro.CompanyRepository;
import br.com.logpro.pesquisa.service.AbstractCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyService extends AbstractCrudService<Company, Integer> {

    private CompanyRepository repository;

    @Autowired
    public CompanyService(CompanyRepository repository) {
        super(repository);
        this.repository = repository;
    }
}
