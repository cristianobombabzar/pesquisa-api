package br.com.logpro.pesquisa.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.logpro.pesquisa.filter.Filter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;



public abstract class AbstractCrudService<T, ID extends Serializable> implements ICrudService<T, ID> {

    private final JpaRepository<T, ID> repository;

    public AbstractCrudService(JpaRepository<T, ID> repository){
        this.repository = repository;
    }

    @Override
    public Page<T> findAll(Pageable pageable){
        return repository.findAll(pageable);
    }

    @Override
    public ResponseEntity<T> findOne(ID id){
        T entity = repository.findOne(id);
        if (entity == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(repository.findOne(id));
    }

    @Override
    public void delete(ID id){
        repository.delete(id);
    }

    @Override
    public ResponseEntity<T> save(T entity) {
        T newEntity = repository.save(entity);
        if (newEntity != null){
            return ResponseEntity.status(HttpStatus.CREATED).body(newEntity);
        }else{
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

    @Override
    public ResponseEntity<T> save(ID id, T entity){
        T entitySaved = repository.findOne(id);
        if (entitySaved == null){
            return ResponseEntity.notFound().build();
        }
        BeanUtils.copyProperties(entity, entitySaved, "id");
        return ResponseEntity.ok(repository.save(entitySaved));
    }
}
