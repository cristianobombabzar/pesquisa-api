package br.com.logpro.pesquisa.service.cadastro;

import br.com.logpro.pesquisa.model.cadastro.Company;
import br.com.logpro.pesquisa.model.cadastro.CompanyAddress;
import br.com.logpro.pesquisa.repository.cadastro.CompanyAddressRepository;
import br.com.logpro.pesquisa.service.AbstractCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CompanyAddressService extends AbstractCrudService<CompanyAddress, Integer>  {

    CompanyAddressRepository repository;

    @Autowired
    public CompanyAddressService(CompanyAddressRepository repository) {
        super(repository);
        this.repository = repository;
    }

    public Optional<CompanyAddress> findByCompany(Company company){
        return repository.findByCompany(company);
    }

}
