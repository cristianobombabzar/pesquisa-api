package br.com.logpro.pesquisa.service;

import br.com.logpro.pesquisa.filter.Filter;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Class is a generic service for all over services system.
 * @param <T>
 * @param <ID>
 */
public interface ICrudService<T, ID> {

    Page<T> findAll(Pageable pageable);
    ResponseEntity<T> findOne(ID id);
    void delete(ID id);
    ResponseEntity<T> save(T entity);
    ResponseEntity<T> save(ID id, T entity);

}
