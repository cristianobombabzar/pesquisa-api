package br.com.logpro.pesquisa.resource;

import br.com.logpro.pesquisa.filter.Filter;
import br.com.logpro.pesquisa.service.ICrudService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCrudResource<T, ID extends Serializable> implements ICrudResource<T, ID> {

    private ICrudService<T, ID> service;

    public AbstractCrudResource(ICrudService<T, ID> service) {
        this.service = service;
    }

    @Override
    @GetMapping
    public Page<T> findAll(Pageable pageable){
        Page<T> page = service.findAll(pageable);
        return page;
    }

    @Override
    @GetMapping("/{id}")
    public ResponseEntity<T> findOne(@PathVariable  ID id) {
        return service.findOne(id);
    }

    @Override
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable ID id){
        service.delete(id);
    }

    @PostMapping
    public ResponseEntity<T> save(@RequestBody @Valid T entity){
        return service.save(entity);
    }

    @PutMapping("/{id}")
    public ResponseEntity<T> save(@PathVariable ID id, @RequestBody @Valid T entity){
        return service.save(id, entity);
    }

}
