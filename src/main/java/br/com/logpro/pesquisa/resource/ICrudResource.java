package br.com.logpro.pesquisa.resource;

import br.com.logpro.pesquisa.filter.Filter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface ICrudResource<T, ID> {

    Page<T> findAll(Pageable pageable);
    ResponseEntity<T> findOne(ID id);
    void delete(ID id);
    ResponseEntity<T> save(T entity);
    ResponseEntity<T> save(ID id, T entity);

}
