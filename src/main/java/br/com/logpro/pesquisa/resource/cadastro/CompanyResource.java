package br.com.logpro.pesquisa.resource.cadastro;

import br.com.logpro.pesquisa.model.cadastro.Company;
import br.com.logpro.pesquisa.model.cadastro.CompanyAddress;
import br.com.logpro.pesquisa.resource.AbstractCrudResource;
import br.com.logpro.pesquisa.service.cadastro.CompanyAddressService;
import br.com.logpro.pesquisa.service.cadastro.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/company")
public class CompanyResource extends AbstractCrudResource<Company, Integer> {

    CompanyService service;
    CompanyAddressService companyAddressService;

    @Autowired
    public CompanyResource(CompanyService service, CompanyAddressService companyAddressService) {
        super(service);
        this.service  = service;
        this.companyAddressService = companyAddressService;
    }

    @GetMapping("/{id}/address")
    public ResponseEntity<CompanyAddress> findAddress(@PathVariable Integer id, Pageable pageable){
         Company company = Company.builder().id(id).build();
         CompanyAddress companyAddress = companyAddressService.findByCompany(company).orElseThrow( () -> new RuntimeException("Endereço não encontrado"));
        return ResponseEntity.ok(companyAddress);
    }

    @PostMapping("/{id}/address")
    public ResponseEntity<CompanyAddress> saveAddress(@PathVariable Integer id, @RequestBody CompanyAddress companyAddress){
        Company company = Company.builder().id(id).build();
        companyAddress.setCompany(company);
        return companyAddressService.save(companyAddress);
    }

    @PutMapping("/{id}/address")
    public ResponseEntity<CompanyAddress> updateAddress(@PathVariable Integer id, @RequestBody CompanyAddress companyAddress){
        Company company = Company.builder().id(id).build();
        companyAddress.setCompany(company);
        return companyAddressService.save(companyAddress);
    }

    @DeleteMapping("/{id}/address")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void  deleteAddress(@PathVariable Integer id){
        Company company = Company.builder().id(id).build();
        CompanyAddress companyAddress = companyAddressService.findByCompany(company).orElseThrow( () -> new RuntimeException("Endereço não encontrado"));
        companyAddressService.delete(companyAddress.getId());
    }
}
