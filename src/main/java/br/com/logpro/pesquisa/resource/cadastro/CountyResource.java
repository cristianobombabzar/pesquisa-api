package br.com.logpro.pesquisa.resource.cadastro;

import br.com.logpro.pesquisa.model.cadastro.County;
import br.com.logpro.pesquisa.resource.AbstractCrudResource;
import br.com.logpro.pesquisa.service.cadastro.CountyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/county")
public class CountyResource extends AbstractCrudResource<County, Integer> {

    private CountyService service;

    @Autowired
    public CountyResource(CountyService service) {
        super(service);
        this.service = service;
    }
}
